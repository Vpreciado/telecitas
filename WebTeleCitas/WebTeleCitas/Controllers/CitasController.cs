﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Model;
using Model.Entidad;
using Service;
using WebTeleCitas.ViewModels;

namespace WebTeleCitas.Controllers
{
    public class CitasController : Controller
    {
        private readonly IPersonaService _servicePer;
        private readonly IPacienteService _servicePaci;
        private readonly IErrorService _serviceError;
        private readonly IEspecialidadService _serviceEspeci;

        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;

        public CitasController(IPersonaService servicePer, IPacienteService servicePaci, IErrorService serviceError,
                                IHttpContextAccessor httpContextAccessor, IEspecialidadService serviceEspeci)
        {
            this._servicePer = servicePer;
            this._servicePaci = servicePaci;
            this._serviceError = serviceError;
            _serviceEspeci = serviceEspeci;

            _httpContextAccessor = httpContextAccessor;
        }

        public List<VmFamiliar> ObtenerDependientes(int idpersona)
        {
            //var familiares = new VmCitas().listaDependiente;
            var familiares = new List<VmFamiliar>();
            int idpaTitular = _servicePaci.GetPacienteXIdPersona(idpersona).ID;

            _servicePaci.GetPacientesXIdTitular(idpaTitular).ForEach(u =>
            {
                var datoper = _servicePer.GetPersona(u.IDSYS00);
                var vm = new VmFamiliar();
                vm.NCOMPLETO = datoper.APATERNO + " " + datoper.AMATERNO + " " + datoper.PNOMBRE + " " + datoper.SNOMBRE;
                vm.NPRNTSCO = Parentesco.descripcion(u.IDPRNTSCO);
                familiares.Add(vm);
            });

            return familiares;
        }

        public string ListaEspecialidad()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var type in _serviceEspeci.GetEspecialidades())
            {
                sb.Append("<option value='" + type.ID + "'>" + type.DSPCLDD + "</option>");
            }

            return sb.ToString();
        }

        public IActionResult Index()
        {
            //obtener idpersona en session
            var idpersona = _session.GetString("idpersona");
            if (idpersona == null)
            {
                return RedirectToAction("Login", "Persona");
            }

            //Titular
            Persona per = _servicePer.GetPersona(int.Parse(idpersona));
            ViewBag.TitularNombre = per.APATERNO + " " + per.AMATERNO + " " + per.PNOMBRE + " " + per.SNOMBRE;

            //Dependientes
            List<VmFamiliar> lstdependiente = ObtenerDependientes(int.Parse(idpersona));
            // new VmCitas().listaDependiente = ObtenerDependientes(int.Parse(idpersona));

            //Especialidades
            ViewBag.Especialidades = ListaEspecialidad();

            return View("../Pages/Citas/Index", lstdependiente);
        }

        [HttpGet]
        public ActionResult AgregarPersonaFamiliar()
        {
            return PartialView("../Pages/Citas/Partial/AgregarPersona");
        }

        [HttpPost]
        public ActionResult AgregarFamiliar(VmFamiliar fam)
        {

            ViewBag.CitaMensaje = null;

            //valida parentesco
            if (fam.IDPRNTSCO == null)
            {
                ViewBag.CitaMensaje = Mensajes.PARENTESCO_NO_VALIDO;
                return PartialView("../Pages/Citas/Partial/AgregarPersona");
            }

            try
            {
                #region registro persona
                Persona per = new Persona();
                per = _servicePer.GetPersonasXTipNumero(fam.TDCMNTO, fam.NDCMNTO);

                //validacion tipo y numero documento en personas
                if (per == null)
                {
                    //ViewBag.CitaMensaje = Mensajes.TIPO_DOCUMENTO_EXISTE;
                    //return PartialView("../Pages/Citas/Partial/AgregarPersona");

                    per.PNOMBRE = fam.PNOMBRE.ToUpper();
                    per.SNOMBRE = fam.SNOMBRE == null ? null : fam.SNOMBRE.ToUpper();
                    per.APATERNO = fam.APATERNO.ToUpper();
                    per.AMATERNO = fam.AMATERNO.ToUpper();
                    per.FNCMNTO = DateTime.Now.AddYears(-40); // cambiar cuando se valide campo
                    per.SXO = fam.SXO;
                    per.TDCMNTO = fam.TDCMNTO;
                    per.NDCMNTO = fam.NDCMNTO;
                    per.CRRO = fam.NDCMNTO;

                    //registrar persona
                    _servicePer.InsertPersona(per);
                }
                #endregion

                #region codigo paciente titular
                var idpersona = _session.GetString("idpersona");
                int idpaTitular = _servicePaci.GetPacienteXIdPersona(int.Parse(idpersona)).ID;
                #endregion

                //se debe validar si existe para mismo titular(si esta inactivo, mostrar mensaje si desea activarlo), 
                //caso contrario mencionarle que ya esta registrado como activo
                #region validacion 1
                // obtener pacientes del titular
                List<Paciente> lstpacientes = _servicePaci.GetPacientesXIdTitular(idpaTitular).Where(x => x.IDSYS00 == per.ID).ToList();

                if (lstpacientes.Count > 0)
                {
                    if (lstpacientes[0].CESTDO != Codigos.ESTADO_A)
                    {
                        ViewBag.CitaMensaje = Mensajes.PACIENTE_INACTIVO;
                        //return
                    }
                    else
                    {
                        ViewBag.CitaMensaje = Mensajes.PACIENTE_REGISTRADO_MISMO_TITULAR;
                        //return
                    }
                }
                #endregion

                //registrar paciente
                Paciente pa = new Paciente();
                pa.IDSYS00 = per.ID;
                pa.IDPRNTSCO = fam.IDPRNTSCO;
                pa.NHCLNCA = null;
                pa.IDTTLR = idpaTitular;
                _servicePaci.InsertPaciente(pa);

                //una persona puede ser dependiente para dos titulares (mostrarle aviso que existe en otro titular)
                #region validacion 2
                if ((_servicePaci.GetPacienteXIdPersona(per.ID)) != null)
                {
                    ViewBag.CitaMensajeR = Mensajes.PACIENTE_EXISTE_OTRO_TITULAR + " " + Mensajes.AGREGA_CORRECTO;
                    //return
                }
                else
                {
                    ViewBag.CitaMensajeR = Mensajes.AGREGA_CORRECTO;
                }
                #endregion
                

            }
            catch (Exception ex)
            {
                Errores("Persona", "Insertar", ex.Message.ToString());
                return PartialView("../Pages/Citas/Partial/AgregarPersona");
            }

            //return PartialView("../Pages/Citas/Index",per);
            return RedirectToAction("Index", "Citas");
            //return View("../Pages/Citas/Index");
        }

        public void Errores(string tabla, string paquete, string descrip)
        {
            Error e = new Error();
            e.NTBLA = tabla;
            e.NPKGPD = paquete;
            e.DSCRPCN = descrip;
            _serviceError.InsertError(e);
            //return RedirectToAction(action, "Error", e);
        }
    }

    
}