﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTeleCitas.ViewModels
{
    public class VmFamiliar
    {
        public int ID { get; set; }
        public string PNOMBRE { get; set; }
        public string SNOMBRE { get; set; }
        public string APATERNO { get; set; }
        public string AMATERNO { get; set; }
        public DateTime FNCMNTO { get; set; }
        public string SXO { get; set; }
        public string TDCMNTO { get; set; }
        public string NDCMNTO { get; set; }

        public string IDPRNTSCO { get; set; }

        public string NCOMPLETO { get; set; }
        public string NPRNTSCO { get; set; }
        
    }

}
