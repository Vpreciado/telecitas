﻿using Model;
using Model.Crud;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Service
{
    public interface IParametroService
    {
        List<Parametro> GetParametros();
        Parametro GetParametro(int id);
        void InsertParametro(Parametro user);
        void UpdateParametro(Parametro user);
    }

    public class ParametroService : IParametroService
    {
        private readonly ICrudParametro<Parametro> Crud_Par;
        public ParametroService(ICrudParametro<Parametro> _Crud_Par)
        {
            this.Crud_Par = _Crud_Par;
        }

        public Parametro GetParametro(int id)
        {
            return Crud_Par.Get(id);
        }

        public List<Parametro> GetParametros()
        {
            return Crud_Par.GetAll().ToList();
        }

        public void InsertParametro(Parametro user)
        {
            user.FACTLZCN = DateTime.Now;
            user.FCRCN = DateTime.Now;
            user.FESTDO = DateTime.Now;
            user.UCRCN = Codigos.USUARIO;
            user.UACTLZCN = Codigos.USUARIO;
            user.CESTDO = Codigos.ESTADO_A;
            Crud_Par.Insert(user);
        }

        public void UpdateParametro(Parametro user)
        {
            Crud_Par.Update(user);
        }
    }
}
