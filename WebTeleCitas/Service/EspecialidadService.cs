﻿using Model;
using Model.Crud;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Service
{
    public interface IEspecialidadService
    {
        List<Especialidad> GetEspecialidades();
        Especialidad GetEspecialidad(int id);
        void InsertEspecialidad(Especialidad user);
        void UpdateEspecialidad(Especialidad user);
    }

    public class EspecialidadService : IEspecialidadService
    {
        private readonly ICrudEspecialidad<Especialidad> Crud_Espec;

        public EspecialidadService(ICrudEspecialidad<Especialidad> _Crud_Espec)
        {
            this.Crud_Espec = _Crud_Espec;
        }

        public Especialidad GetEspecialidad(int id)
        {
            return Crud_Espec.Get(id);
        }

        public List<Especialidad> GetEspecialidades()
        {
            return Crud_Espec.GetAll().ToList();
        }

        public void InsertEspecialidad(Especialidad user)
        {
            user.FACTLZCN = DateTime.Now;
            user.FCRCN = DateTime.Now;
            user.FESTDO = DateTime.Now;
            user.UCRCN = Codigos.USUARIO;
            user.UACTLZCN = Codigos.USUARIO;
            user.CESTDO = Codigos.ESTADO_A;
            Crud_Espec.Insert(user);
        }

        public void UpdateEspecialidad(Especialidad user)
        {
            Crud_Espec.Update(user);
        }
    }
}
