﻿using Model;
using Model.Crud;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Service
{
    public interface IErrorService
    {
        List<Error> GetErrors();
        Error GetError(int id);
        void InsertError(Error user);
        void UpdateError(Error user);
    }

    public class ErrorService : IErrorService
    {
        private readonly ICrudError<Error> Crud_Err;
        public ErrorService(ICrudError<Error> _Crud_Err)
        {
            this.Crud_Err = _Crud_Err;
        }

        public Error GetError(int id)
        {
            return Crud_Err.Get(id);
        }

        public List<Error> GetErrors()
        {
            return Crud_Err.GetAll().ToList();
        }

        public void InsertError(Error user)
        {
            user.FACTLZCN = DateTime.Now;
            user.FCRCN = DateTime.Now;
            user.FESTDO = DateTime.Now;
            user.UCRCN = Codigos.USUARIO;
            user.UACTLZCN = Codigos.USUARIO;
            user.CESTDO = Codigos.ESTADO_A;
            Crud_Err.Insert(user);
        }

        public void UpdateError(Error user)
        {
            Crud_Err.Update(user);
        }
    }
}
