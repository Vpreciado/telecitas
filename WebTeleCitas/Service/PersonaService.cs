﻿using Model;
using Model.Crud;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Service
{
    public interface IPersonaService
    {
        List<Persona> GetPersonas();
        Persona GetPersona(int id);
        void InsertPersona(Persona user);
        void UpdatePersona(Persona user);
        Persona GetPersonaSession(string tipo, string numero, string clave);
        Persona GetPersonasXCorreo(string correo);
        Persona GetPersonasXTipNumero(string tipo, string numero);
    }

    public class PersonaService : IPersonaService
    {
        private readonly ICrudPersona<Persona> Crud_Per;
        public PersonaService(ICrudPersona<Persona> _Crud_Per)
        {
            this.Crud_Per = _Crud_Per;
        }

        public Persona GetPersona(int id)
        {
            return Crud_Per.Get(id);
        }

        public List<Persona> GetPersonas()
        {
            return Crud_Per.GetAll().ToList();
        }

        public Persona GetPersonaSession(string tipo, string numero, string clave)
        {
            return Crud_Per.Login(tipo, numero, Contrasena.Encriptar(clave));
        }

        public Persona GetPersonasXCorreo(string correo)
        {
            return Crud_Per.GetXCorreo(correo);
        }

        public Persona GetPersonasXTipNumero(string tipo, string numero)
        {
            return Crud_Per.GetXTipNumero(tipo, numero);
        }

        public void InsertPersona(Persona user)
        {
            user.FACTLZCN = DateTime.Now;
            user.FCRCN = DateTime.Now;
            user.FESTDO = DateTime.Now;
            user.UCRCN = Codigos.USUARIO;
            user.UACTLZCN = Codigos.USUARIO;
            user.CESTDO = Codigos.ESTADO_A;

            if (user.CNTRSNA != null)
            {
                user.CNTRSNA = Contrasena.Encriptar(user.CNTRSNA);
            }
            Crud_Per.Insert(user);
        }

        public void UpdatePersona(Persona user)
        {
            user.FACTLZCN = DateTime.Now;
            user.UACTLZCN = Codigos.USUARIO;

            user.CNTRSNA = Contrasena.Encriptar(user.CNTRSNA);

            Crud_Per.Update(user);
        }


    }
}
