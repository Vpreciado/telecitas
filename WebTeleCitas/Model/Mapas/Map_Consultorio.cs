﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Mapas
{
    public class Map_Consultorio
    {
        public Map_Consultorio(EntityTypeBuilder<Consultorio> entity)
        {
            entity.HasKey(e => e.Id);

            entity.ToTable("GCSYS06", "CIT");

            entity.Property(e => e.Id)
                .HasColumnName("ID")
                .ValueGeneratedOnAdd();

            entity.Property(e => e.Piso)
                .IsRequired()
                .HasColumnName("PISO")
                .IsUnicode(false);

            entity.Property(e => e.DConsultorio)
                .IsRequired()
                .HasColumnName("DCNSLTRO")
                .HasMaxLength(60)
                .IsUnicode(false);
        }
    }
}
