﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Entidad
{
    public class Paciente : Auditoria
    {
        public int ID { get; set; }
        public int IDSYS00 { get; set; }
        public string IDPRNTSCO { get; set; }
        public string NHCLNCA { get; set; }
        public int IDTTLR { get; set; }
    }
}
