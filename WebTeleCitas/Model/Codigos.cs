﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public static class Codigos
    {
        public const string USUARIO = "CEPHEID";
        public const string ESTADO_A = "A";
        public const string CORREO = "vpreciado@cepheid.info";
        public const string CLAVE_CORREO = "cepheid123";
        public const int EDAD = 18;
        public const string CODTITULAR = "1";
    }
    public static class IconoBoton
    {
        public const string Editar = "glyphicon glyphicon-pencil";
        public const string Agregar = "glyphicon glyphicon-plus";
        public const string Eliminar = "glyphicon glyphicon-trash";
        public const string Ver = "glyphicon glyphicon-eye-open";
        public const string Cerrar = "glyphicon glyphicon-plus";
    }
    public static class ColorBoton
    {
        public const string Default = "btn btn-default btn-xs";
        public const string Principal = "btn btn-primary btn-xs";
        public const string Info = "btn btn-info btn-xs";
        public const string Exito = "btn btn-success btn-xs";
        public const string Advertencia = "btn btn-warning btn-xs";
        public const string Error = "btn btn-danger btn-xs";
    }
    public class Validacion
    {
        public bool CalcularEdad(DateTime fechanaci)
        {
            int edad = DateTime.Today.AddTicks(-fechanaci.Ticks).Year - 1;

            if (edad >= Codigos.EDAD)
                return true;
            else
                return false;
                    
        }
    }

    public static class Parentesco
    {
        public static string descripcion(string codigo)
        {
            string rpta = string.Empty;
            switch (codigo)
            {
                case "2":
                    rpta = "Padre";
                    break;
                case "3":
                    rpta = "Hijo(a)";
                    break;
                case "4":
                    rpta = "Conyuge";
                    break;
                case "5":
                    rpta = "Hermano(a)";
                    break;
                case "6":
                    rpta = "Madre";
                    break;
                default:
                    rpta = "Otro";
                    break;
            }

            return rpta;
        }
    }
}
