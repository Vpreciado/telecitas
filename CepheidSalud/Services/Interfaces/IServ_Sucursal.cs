﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
   public interface IServ_Sucursal
    {
        List<Sucursal> GetSucursales();
        List<Sucursal> ObtenerSucursalxEmpresa(decimal id);
        Sucursal GetSucursal(decimal id);
        void InsertSucursal(Sucursal user);
        void UpdateSucursal(Sucursal user);
    }
}
