﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IServ_Perfil
    {
        List<Perfil> GetPerfils();
        List<Perfil> ObtenerPerfilXModuloXSistema(decimal id, decimal idM);
        Perfil GetPerfil(long id);
        void InsertPerfil(Perfil user);
        void UpdatePerfil(Perfil user);
    }
}
