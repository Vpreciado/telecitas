﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;

namespace Services.Interfaces
{
    public interface IServ_OpcionDetalle
    {
        List<OpcionDetalle> GetOpcionDetalles();
        List<OpcionDetalle> ObtenerOpcionDetalleXModuloXSistema(decimal id, decimal idM,decimal idOp);
        OpcionDetalle GetOpcionDetalle(long id);
        void InsertOpcionDetalle(OpcionDetalle user);
        void UpdateOpcionDetalle(OpcionDetalle user);
    }
}
