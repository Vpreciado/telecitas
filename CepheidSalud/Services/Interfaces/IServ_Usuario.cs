﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
   public interface IServ_Usuario
    {
        List<Usuario> GetUsuarios();
        Usuario GetUsuario(long id);
        void InsertUsuario(Usuario user);
        void UpdateUsuario(Usuario user);
    }
}
