﻿using Models;
using Repo.Interfaces;
using Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Services.Classes
{
    public class Serv_OpcionDetalle : IServ_OpcionDetalle
    {
        private IRepo<OpcionDetalle> Repo_OpDet;

        public Serv_OpcionDetalle(IRepo<OpcionDetalle> Repo_OpDet)
        {
            this.Repo_OpDet = Repo_OpDet;
        }

        public List<OpcionDetalle> GetOpcionDetalles()
        {
            return Repo_OpDet.GetAll().ToList();
        }

        public OpcionDetalle GetOpcionDetalle(long id)
        {
            return Repo_OpDet.Get(id);
        }

        public void InsertOpcionDetalle(OpcionDetalle user)
        {
            user.IODtlle = ObtenerOpcionDetalleXModuloXSistema(user.ISstma, user.IMdlo, user.IOpcn).Count() + 1;
            Repo_OpDet.Insert(user);
        }
        public void UpdateOpcionDetalle(OpcionDetalle user)
        {
            Repo_OpDet.Update(user);
        }

        public List<OpcionDetalle> ObtenerOpcionDetalleXModuloXSistema(decimal id, decimal idM, decimal idOpDet)
        {
            return GetOpcionDetalles().Where(x => x.ISstma == id && x.IMdlo == idM && x.IOpcn == idOpDet).ToList();
        }

    }
}
