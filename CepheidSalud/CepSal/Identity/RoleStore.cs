﻿using Microsoft.AspNetCore.Identity;
using Models;
using Services.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CepSal.Identity
{
    public interface IAppRoleStore : IRoleStore<Usuario>
    {
        Task<string> GetRoleIdAsync(Usuario role, decimal ISistema, CancellationToken cancellationToken);
    }
    public class RoleStore : IAppRoleStore
    {
        private readonly IServ_Perfil _servPerfil;
        private readonly IServ_UsuarioPerfil _servUsuarioPerfil;
        public RoleStore(IServ_UsuarioPerfil ServUsuarioPerfil, IServ_Perfil ServPerfil)
        {
            this._servPerfil = ServPerfil;
            this._servUsuarioPerfil = ServUsuarioPerfil;
        }
        public void Dispose()
        {
        }

        public Task<IdentityResult> CreateAsync(Usuario role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> UpdateAsync(Usuario role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> DeleteAsync(Usuario role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetRoleIdAsync(Usuario role, decimal ISistema, CancellationToken cancellationToken)
        {
            return Task.FromResult(_servUsuarioPerfil.GetUsuarioPerfils().Where(x => x.IEmprsa == role.IEmprsa && x.IPrsna == role.IPrsna && x.ISstma == ISistema).FirstOrDefault().IPrfl.ToString());
        }
        public Task<string> GetRoleIdAsync(Usuario role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetRoleNameAsync(Usuario role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task SetRoleNameAsync(Usuario role, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetNormalizedRoleNameAsync(Usuario role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task SetNormalizedRoleNameAsync(Usuario role, string normalizedName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<Usuario> FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<Usuario> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public void GetRoleByUser(Usuario usr)
        {
            _servUsuarioPerfil.GetUsuarioPerfils();
        }
    }
}
