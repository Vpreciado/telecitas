﻿using Microsoft.AspNetCore.Identity;
using Models;
using Services.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace CepSal.Identity
{
    public interface IAppUserStore : IUserStore<Usuario>, IUserPasswordStore<Usuario>
    {
      
    }
    public class AppUserStore : IAppUserStore
    {
        private readonly IServ_Usuario _servUsuario;
        public AppUserStore(IServ_Usuario UserRepository)
        {
            this._servUsuario = UserRepository;
        }
        #region IUserStore
        public Task<IdentityResult> CreateAsync(Usuario user, CancellationToken cancellationToken)
        {
            _servUsuario.InsertUsuario(new Usuario
            {
                IEmprsa = 1,
                IPrsna = 2,
                CUsro = user.CUsro,
                UCrcn = "CEPHEID",
                UActlzcn = "CEPHEID",
                Cntrsna = user.Cntrsna,
                CCCntrsna = "S",
            });
            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> DeleteAsync(Usuario user, CancellationToken cancellationToken)
        {
            var appUser = _servUsuario.GetUsuarios().FirstOrDefault(u => u.nId == user.nId);

            if (appUser != null)
            {
                //UserRepository.Users.Remove(appUser);
            }

            return Task.FromResult(IdentityResult.Success);
        }

        public void Dispose()
        {
            // throw new NotImplementedException();
        }

        public Task<Usuario> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            return Task.FromResult(_servUsuario.GetUsuarios().FirstOrDefault(u => u.nId == Convert.ToInt32(userId)));
        }

        public Task<Usuario> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            return Task.FromResult(_servUsuario.GetUsuarios().FirstOrDefault(u => u.CUsro == normalizedUserName));
        }

        public Task<string> GetNormalizedUserNameAsync(Usuario user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.CUsro);
        }

        public Task<string> GetUserIdAsync(Usuario user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.nId.ToString());
        }

        public Task<string> GetUserNameAsync(Usuario user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.CUsro);
        }

        public Task SetNormalizedUserNameAsync(Usuario user, string normalizedName, CancellationToken cancellationToken)
        {
            user.CUsro = normalizedName;
            return Task.CompletedTask;
        }

        public Task SetUserNameAsync(Usuario user, string userName, CancellationToken cancellationToken)
        {
            user.CUsro = userName;
            return Task.CompletedTask;
        }

        public Task<IdentityResult> UpdateAsync(Usuario user, CancellationToken cancellationToken)
        {
            var appUser = _servUsuario.GetUsuarios().FirstOrDefault(u => u.nId == user.nId);

            if (appUser != null)
            {
                appUser.CUsro = user.CUsro;
                //appUser.UserName = user.UserName;
                //appUser.Email = user.Email;
                appUser.Cntrsna = user.Cntrsna;
            }

            return Task.FromResult(IdentityResult.Success);
        }

        #endregion

        #region IUserPasswordStore
        public Task<bool> HasPasswordAsync(Usuario user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Cntrsna != null);
        }

        public Task<string> GetPasswordHashAsync(Usuario user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Cntrsna);
        }

        public Task SetPasswordHashAsync(Usuario user, string passwordHash, CancellationToken cancellationToken)
        {
            user.Cntrsna = passwordHash;
            return Task.CompletedTask;
        }
        #endregion
    }
}
