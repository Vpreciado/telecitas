﻿using System.Collections.Generic;

namespace CepSal.ViewModels
{
    public class ModulosMenu
    {
        public string Nombre { get; set; }
        public List<OpcionPadreMenu> ListaOpcionesPadres { get; set; }
    }
    public class OpcionHijoMenu
    {
        public string Controlador { get; set; }
        public string Accion { get; set; }
        public string Nombre { get; set; }
    }
    public class OpcionPadreMenu
    {
        public string Url { get; set; }
        public string Nombre { get; set; }
        public string Icono { get; set; }
        public List<OpcionHijoMenu> ListaOpcionesHijo { get; set; }
    }
    public class vmMenuLateral
    {
        //Lista de modulos
        public List<ModulosMenu> ListaModulos { get; set; }
    }
}
