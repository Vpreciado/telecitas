﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CepSal.ViewModels
{
    public class vmSucursal : vmAuditoria
    {
        public static implicit operator vmSucursal(Sucursal u)
        {
            return new vmSucursal()
            {
                nId = u.nId,
                IEmprsa = Convert.ToInt32(u.IEmprsa),
                IScrsl = Convert.ToInt32(u.IScrsl),
                Nmbre = u.Nmbre,
                Dscrpcn = u.Dscrpcn,
                CEstdo = u.CEstdo,
                FEstdo = u.FEstdo,
                FActlzcn = u.FActlzcn,
                UActlzcn = u.UActlzcn,
                UCrcn = u.UCrcn,
                FCrcn = u.FCrcn
            };
        }
        public static implicit operator Sucursal(vmSucursal u)
        {
            return new Sucursal
            {
                nId = u.nId,
                IEmprsa = u.IEmprsa,
                IScrsl = u.IScrsl,
                Nmbre = u.Nmbre,
                Dscrpcn = u.Dscrpcn,
                CEstdo = u.CEstdo,
                FEstdo = u.FEstdo,
                FActlzcn = u.FActlzcn,
                UActlzcn = u.UActlzcn,
                UCrcn = u.UCrcn,
                FCrcn = u.FCrcn
            };
        }

        public vmSucursal() : base()
        {

        }
        public vmSucursal(int id) : base()
        {
            IEmprsa = id;
        }

        [Display(Name = "Id")]
        [Required(ErrorMessage = "Seleccione una sucursal.")]
        public int nId { get; set; }
        [Display(Name = "Empresa")]
        [Required(ErrorMessage = "Seleccione una empresa.")]
        public int IEmprsa { get; set; }
        public decimal IScrsl { get; set; }
        [Display(Name = "Nombre sucursal")]
        [Required(ErrorMessage = "Por favor indique un nombre para la sucursal.")]
        public string Nmbre { get; set; }
        [Display(Name = "Descripción")]
        [Required(ErrorMessage = "Por favor indique una desripción para la sucursal.")]
        public string Dscrpcn { get; set; }

        public List<OptionItems> Empresas { get; set; }

    }
    public class vmBuscarSucursales
    {
        public int idEmpresa { get; set; }
    }
    public class vmListadoSucursales
    {
        public int idEmpresa { get; set; }
        public List<vmSucursal> Sucursales { get; set; }
    }
}
