﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Models;

namespace CepSal.ViewModels
{
    public class vmModulo : vmAuditoria
    {
        public static implicit operator vmModulo(Modulo u)
        {
            return new vmModulo()
            {
                nId = u.nId,
                ISstma = Convert.ToInt32(u.ISstma),
                IMdlo = Convert.ToInt32(u.IMdlo),
                Nmbre = u.Nmbre,
                Dscrpcn = u.Dscrpcn,
                CEstdo = u.CEstdo,
                FEstdo = u.FEstdo,
                FActlzcn = u.FActlzcn,
                UActlzcn = u.UActlzcn,
                UCrcn = u.UCrcn,
                FCrcn = u.FCrcn
            };
        }
        public static implicit operator Modulo(vmModulo u)
        {
            return new Modulo
            {
                nId = u.nId,
                ISstma = u.ISstma,
                IMdlo = u.IMdlo,
                Nmbre = u.Nmbre,
                Dscrpcn = u.Dscrpcn,
                CEstdo = u.CEstdo,
                FEstdo = u.FEstdo,
                FActlzcn = u.FActlzcn,
                UActlzcn = u.UActlzcn,
                UCrcn = u.UCrcn,
                FCrcn = u.FCrcn
            };
        }
        public vmModulo() : base()
        {
        }
        public vmModulo(int id) : base()
        {
            ISstma = id;
        }

        [Display(Name = "Id")]
        [Required(ErrorMessage = "Seleccione un Módulo.")]
        public int nId { get; set; }
        [Display(Name = "Sistema")]
        [Required(ErrorMessage = "Seleccione un Sistema.")]
        public int ISstma { get; set; }
        public decimal IMdlo { get; set; }
        [Display(Name = "Nombre Módulo")]
        [Required(ErrorMessage = "Por favor indique un nombre para el módulo.")]
        public string Nmbre { get; set; }
        [Display(Name = "Descripción")]
        [Required(ErrorMessage = "Por favor indique una desripción para el módulo.")]
        public string Dscrpcn { get; set; }

        public List<OptionItems> Sistemas { get; set; }
    }

    public class vmBuscarModulos
    {
        public int idsistema { get; set; }
    }
    public class vmListadoModulos
    {
        public int idsistema { get; set; }
        public List<vmModulo> Modulos { get; set; }
    }
}
