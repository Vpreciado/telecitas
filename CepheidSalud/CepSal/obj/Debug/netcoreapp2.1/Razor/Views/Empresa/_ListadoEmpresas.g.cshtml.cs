#pragma checksum "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b9cd7425f69df0c3058cd25e5dc3140f070a5388"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Empresa__ListadoEmpresas), @"mvc.1.0.view", @"/Views/Empresa/_ListadoEmpresas.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Empresa/_ListadoEmpresas.cshtml", typeof(AspNetCore.Views_Empresa__ListadoEmpresas))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using CepSal;

#line default
#line hidden
#line 2 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using Models;

#line default
#line hidden
#line 3 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using CepSal.ViewModels;

#line default
#line hidden
#line 6 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#line 8 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b9cd7425f69df0c3058cd25e5dc3140f070a5388", @"/Views/Empresa/_ListadoEmpresas.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d9a8162adbd3a173fa8bb3c954f6988378f56429", @"/Views/_ViewImports.cshtml")]
    public class Views_Empresa__ListadoEmpresas : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<vmEmpresa>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(24, 200, true);
            WriteLiteral("\r\n<thead>\r\n    <tr>\r\n        <th>Id</th>\r\n        <th>Nombre</th>\r\n        <th>Descripción</th>\r\n        <th>Fecha Act.</th>\r\n        <th>Estado</th>\r\n        <th></th>\r\n    </tr>\r\n</thead>\r\n<tbody>\r\n");
            EndContext();
#line 14 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
     if (Model != null)
    {
        

#line default
#line hidden
#line 16 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
         foreach (var item in Model)
        {

#line default
#line hidden
            BeginContext(305, 38, true);
            WriteLiteral("            <tr>\r\n                <td>");
            EndContext();
            BeginContext(344, 42, false);
#line 19 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
               Write(Html.DisplayFor(modelItem => item.IEmprsa));

#line default
#line hidden
            EndContext();
            BeginContext(386, 27, true);
            WriteLiteral("</td>\r\n                <td>");
            EndContext();
            BeginContext(414, 40, false);
#line 20 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
               Write(Html.DisplayFor(modelItem => item.Nmbre));

#line default
#line hidden
            EndContext();
            BeginContext(454, 27, true);
            WriteLiteral("</td>\r\n                <td>");
            EndContext();
            BeginContext(482, 42, false);
#line 21 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
               Write(Html.DisplayFor(modelItem => item.Dscrpcn));

#line default
#line hidden
            EndContext();
            BeginContext(524, 27, true);
            WriteLiteral("</td>\r\n                <td>");
            EndContext();
            BeginContext(552, 43, false);
#line 22 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
               Write(Html.DisplayFor(modelItem => item.FActlzcn));

#line default
#line hidden
            EndContext();
            BeginContext(595, 29, true);
            WriteLiteral("</td>\r\n                <td>\r\n");
            EndContext();
#line 24 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
                     if (item.CEstdo == "A")
                    {

#line default
#line hidden
            BeginContext(693, 73, true);
            WriteLiteral("                        <span class=\"label label-success\">Activo</span>\r\n");
            EndContext();
#line 27 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
                    }
                    else
                    {

#line default
#line hidden
            BeginContext(838, 74, true);
            WriteLiteral("                        <span class=\"label label-danger\">Inactivo</span>\r\n");
            EndContext();
#line 31 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
                    }

#line default
#line hidden
            BeginContext(935, 458, true);
            WriteLiteral(@"                </td>

                <td>
                    <span class=""tool-tip"" data-toggle=""tooltip"" data-placement=""top"" title=""Editar"">
                        <a id=""edit""
                           data-toggle=""modal""
                           data-target=""#modal-action""
                           data-ajax-success=""success""
                           data-ajax=""true""
                           data-ajax-url=""/Empresa/EditarEmpresa/");
            EndContext();
            BeginContext(1394, 12, false);
#line 41 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
                                                            Write(item.IEmprsa);

#line default
#line hidden
            EndContext();
            BeginContext(1406, 1, true);
            WriteLiteral("\"");
            EndContext();
            BeginWriteAttribute("class", "\r\n                           class=\"", 1407, "\"", 1459, 1);
#line 42 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
WriteAttributeValue("", 1443, ColorBoton.Info, 1443, 16, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1460, 33, true);
            WriteLiteral(">\r\n                            <i");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 1493, "\"", 1519, 1);
#line 43 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
WriteAttributeValue("", 1501, IconoBoton.Editar, 1501, 18, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1520, 66, true);
            WriteLiteral("></i>\r\n                        </a>\r\n                    </span>\r\n");
            EndContext();
#line 46 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
                      
                        var disable = string.Empty;
                        var tooltip = "Eliminar";

                        if (item.TieneSucursales)
                        {
                            disable = "disabled";
                            tooltip = "Para Elimimar, elimine sus sucursales primero";
                        }
                    

#line default
#line hidden
            BeginContext(1983, 20, true);
            WriteLiteral("                    ");
            EndContext();
#line 56 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
                     if ((await AuthorizationService.AuthorizeAsync(User, "SuperUsuario")).Succeeded)
                    {
                        

#line default
#line hidden
            BeginContext(2207, 89, true);
            WriteLiteral("                        <span class=\"tool-tip\" data-toggle=\"tooltip\" data-placement=\"top\"");
            EndContext();
            BeginWriteAttribute("title", " title=\"", 2296, "\"", 2312, 1);
#line 59 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
WriteAttributeValue("", 2304, tooltip, 2304, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2313, 139, true);
            WriteLiteral(">\r\n                            <a id=\"delete\"\r\n                               data-ajax-confirm=\"Está seguro que desea eliminar la empresa ");
            EndContext();
            BeginContext(2453, 10, false);
#line 61 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
                                                                                       Write(item.Nmbre);

#line default
#line hidden
            EndContext();
            BeginContext(2463, 185, true);
            WriteLiteral(" ?\"\r\n                               data-ajax-success=\"success\"\r\n                               data-ajax=\"true\"\r\n                               data-ajax-url=\"/Empresa/EliminarEmpresa/");
            EndContext();
            BeginContext(2649, 12, false);
#line 64 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
                                                                  Write(item.IEmprsa);

#line default
#line hidden
            EndContext();
            BeginContext(2661, 66, true);
            WriteLiteral("\"\r\n                               data-ajax-update=\"#tablaEmpresa\"");
            EndContext();
            BeginWriteAttribute("class", "\r\n                               class=\"", 2727, "\"", 2793, 2);
#line 66 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
WriteAttributeValue("", 2767, ColorBoton.Error, 2767, 17, false);

#line default
#line hidden
#line 66 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
WriteAttributeValue(" ", 2784, disable, 2785, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("title", "\r\n                               title=\"", 2794, "\"", 2842, 1);
#line 67 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
WriteAttributeValue("", 2834, tooltip, 2834, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2843, 37, true);
            WriteLiteral(">\r\n                                <i");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 2880, "\"", 2908, 1);
#line 68 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
WriteAttributeValue("", 2888, IconoBoton.Eliminar, 2888, 20, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2909, 74, true);
            WriteLiteral("></i>\r\n                            </a>\r\n                        </span>\r\n");
            EndContext();
#line 71 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
                    }

#line default
#line hidden
            BeginContext(3006, 41, true);
            WriteLiteral("               </td>\r\n            </tr>\r\n");
            EndContext();
#line 74 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
        }

#line default
#line hidden
#line 74 "D:\GitLab\Proyectos\TeleCitas\CepheidSalud\CepSal\Views\Empresa\_ListadoEmpresas.cshtml"
         
    }

#line default
#line hidden
            BeginContext(3065, 8, true);
            WriteLiteral("</tbody>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IAuthorizationService AuthorizationService { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<vmEmpresa>> Html { get; private set; }
    }
}
#pragma warning restore 1591
