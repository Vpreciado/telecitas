﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Models;
using CepSal.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace CepSal.Controllers
{

    [Authorize(Policy = "TienePermisosSistema")]
    public class SistemaController : Controller
    {
        private readonly IServ_Sistema SistemaServ;
        private readonly IServ_Modulo ModuloServ;
        public SistemaController(IServ_Sistema SistemaServ, IServ_Modulo ModuloServ)
        {
            this.SistemaServ = SistemaServ;
            this.ModuloServ = ModuloServ;
        }

        public List<vmSistema> BuscarSistema()
        {
            var sis = new List<vmSistema>();
            SistemaServ.GetSistemas().ForEach(t =>
            {
                var a = ModuloServ.ObtenerModuloXSistema(t.ISstma).Count();
                var vm = new vmSistema();
                vm.ISstma = Convert.ToInt32(t.ISstma);
                vm.Nmbre = t.Nmbre;
                vm.Dscrpcn = t.Dscrpcn;
                vm.CEstdo = t.CEstdo;
                vm.FEstdo = t.FEstdo;
                vm.FActlzcn = t.FActlzcn;
                vm.UActlzcn = t.UActlzcn;
                vm.UCrcn = t.UCrcn;
                vm.FCrcn = t.FCrcn;
                vm.TieneModulo = (a > 0) ? true : false;
                sis.Add(vm);
            });
            return sis;
        }

        [HttpGet]
        public IActionResult Index()
        {
            List<vmSistema> model = BuscarSistema();
            return View(model);
        }
        [HttpGet]
        public IActionResult AgregarSistema()
        {
            vmSistema model = new vmSistema();
            model.UCrcn = HttpContext.User.Identity.Name;
            model.UActlzcn = HttpContext.User.Identity.Name;
            return PartialView("_AgregarEditarSistema", model);
        }
        public IActionResult EditarSistema(int? id)
        {
            vmSistema model = new vmSistema();
            if (id.HasValue)
            {
                Sistema sis = SistemaServ.GetSistema(id.Value);
                if (sis != null)
                {
                    model = sis;
                    model.UActlzcn = HttpContext.User.Identity.Name;
                    model.FActlzcn = DateTime.Now;
                }
            }
            return PartialView("_AgregarEditarSistema", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AgregarEditarSistema(vmSistema vmSis)
        {
            try
            {
                Sistema sis = null;
                
                if (vmSis.ISstma == 0)
                {
                    
                    sis = vmSis;
                    sis.UActlzcn = HttpContext.User.Identity.Name;
                    sis.UCrcn = HttpContext.User.Identity.Name;
                    SistemaServ.InsertSistema(sis);
                }
                else
                {
                    sis = SistemaServ.GetSistema(vmSis.ISstma);
                    sis.ISstma = vmSis.ISstma;
                    sis.Nmbre = vmSis.Nmbre;
                    sis.Dscrpcn = vmSis.Dscrpcn;
                    sis.CEstdo = vmSis.CEstdo;
                    sis.FEstdo = vmSis.FEstdo;
                    sis.FActlzcn = DateTime.Now;
                    sis.UActlzcn = HttpContext.User.Identity.Name;

                    SistemaServ.UpdateSistema(sis);
                }
                List<vmSistema> model = BuscarSistema();
                return PartialView("_ListadoSistemas", model);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { Mensaje = ex.Message });
            }
        }
        [HttpGet]
        public IActionResult EliminarSistema(int id)
        {
            try
            {
                Sistema sis = SistemaServ.GetSistema(id);
                sis.CEstdo = "I";
                SistemaServ.UpdateSistema(sis);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { Mensaje = ex.Message });
            }

            List<vmSistema> model = BuscarSistema();
            return PartialView("_ListadoSistemas", model);
        }
    }
}