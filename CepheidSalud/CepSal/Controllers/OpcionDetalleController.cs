﻿using CepSal.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CepSal.Controllers
{
    [Authorize(Policy = "TienePermisosOpcionDetalle")]
    public class OpcionDetalleController : Controller
    {
        private readonly IServ_OpcionDetalle OpcionDetalleServ;
        private readonly IServ_Opcion OpcionServ;
        private readonly IServ_Modulo ModuloServ;
        private readonly IServ_Sistema SistemaServ;

        public OpcionDetalleController(IServ_Opcion OpcionServ, IServ_Modulo ModuloServ, IServ_Sistema SistemaServ, IServ_OpcionDetalle OpcionDetalleServ)
        {
            this.OpcionDetalleServ = OpcionDetalleServ;
            this.OpcionServ = OpcionServ;
            this.ModuloServ = ModuloServ;
            this.SistemaServ = SistemaServ;
        }

        public List<vmSistema> Buscarsistema()
        {
            var sis = new List<vmSistema>();
            SistemaServ.GetSistemas().ToList().ForEach(u =>
            {
                sis.Add(u);
            });
            return sis;
        }
        public List<vmModulo> BuscarModulo()
        {
            var sis = new List<vmModulo>();
            ModuloServ.GetModulos().ToList().ForEach(u =>
            {
                sis.Add(u);
            });
            return sis;
        }

        public vmListadoOpciones BuscarOpcionXModuloXSistema(int id,int idMod)
        {
            vmListadoOpciones lis = new vmListadoOpciones();
            lis.Opcions = new List<vmOpcion>();
            lis.idModulo = idMod;
            lis.idsistema = id;
            OpcionServ.ObtenerOpcionXModuloXSistema(id, idMod).ToList().ForEach(u =>
            {
                lis.Opcions.Add(u);
            });
            return lis;
        }

        public vmListadoOpcionesDetalle BuscarOpcionXModuloXSistema(vmBuscarOpcionesDetalle vm)
        {
            vmListadoOpcionesDetalle lis = new vmListadoOpcionesDetalle();
            lis.OpcionsDetalles = new List<vmOpcionDetalle>();
            lis.idsistema = vm.idsistema;
            lis.idModulo = vm.idModulo;
            lis.idOpcion = vm.idOpcion;
            OpcionDetalleServ.ObtenerOpcionDetalleXModuloXSistema(vm.idsistema, vm.idModulo,vm.idOpcion).ToList().ForEach(u =>
            {
                lis.OpcionsDetalles.Add(u);
            });
            //lis.Opcions.ToList().ForEach(u =>
            //{
            //    if (u.IOpcnPdre.HasValue && Convert.ToDecimal(u.IOpcnPdre.Value) != 0)
            //        u.NombrePadre = OpcionServ.ObtenerOpcionXModuloXSistema(vm.idsistema, vm.idModulo).FirstOrDefault(s => s.IOpcn == Convert.ToDecimal(u.IOpcnPdre.Value)).Nmbre;
            //});

            return lis;
        }

        [HttpGet]
        public JsonResult BuscarOpcionXModuloXSistemas(int id, int idMod)
        {
            vmListadoOpciones lis = BuscarOpcionXModuloXSistema(id, idMod);
            return Json(new SelectList(lis.Opcions, "IOpcn", "Nmbre"));
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.Sistemas = Buscarsistema();
            vmBuscarOpcionesDetalle vm = new vmBuscarOpcionesDetalle { idsistema = 0, idModulo = 0, idOpcion = 0 };
            return View(vm);
        }

        [HttpPost]
        public IActionResult BuscarOpcionesDetalleXModuloXSistema(vmBuscarOpcionesDetalle vm)
        {
            vmListadoOpcionesDetalle lis = BuscarOpcionXModuloXSistema(vm);

            return PartialView("_ListadoOpcionesDetalles", lis);
        }

        [HttpGet]
        public IActionResult AgregarOpcionDetalle(int id, int idM, int idOpDet)
        {
            vmOpcionDetalle model = new vmOpcionDetalle(id, idM, idOpDet);
            model.UCrcn = HttpContext.User.Identity.Name;
            model.UActlzcn = HttpContext.User.Identity.Name;
            return PartialView("_AgregarEditarOpcionDetalle", model);
        }
        [HttpGet]
        public IActionResult EditarOpcionDetalle(int id)
        {
            vmOpcionDetalle model = OpcionDetalleServ.GetOpcionDetalle(id);
            return PartialView("_AgregarEditarOpcion", model);
        }

        [HttpGet]
        public IActionResult OpcionDetalle(vmOpcionDetalle item)
        {
            return PartialView("_AgregarEditarOpcionDetalle", null);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AgregarEditarOpcionDetalle(vmOpcionDetalle vmOpcDet)
        {

            try
            {
                OpcionDetalle opcdet = null;
                
                if (vmOpcDet.nId == 0)
                {
                    
                    opcdet = vmOpcDet;
                    opcdet.UActlzcn = HttpContext.User.Identity.Name;
                    opcdet.UCrcn = HttpContext.User.Identity.Name;
                    OpcionDetalleServ.InsertOpcionDetalle(opcdet);
                }
                else
                {
                    opcdet = OpcionDetalleServ.GetOpcionDetalle(vmOpcDet.nId);
                    opcdet.nId = vmOpcDet.nId;
                    opcdet.ISstma = vmOpcDet.ISstma;
                    opcdet.IMdlo = vmOpcDet.IMdlo;
                    opcdet.IOpcn = vmOpcDet.IOpcn;
                    opcdet.Nmbre = vmOpcDet.Nmbre;
                    opcdet.CEstdo = vmOpcDet.CEstdo;
                    opcdet.FEstdo = vmOpcDet.FEstdo;
                    opcdet.FActlzcn = DateTime.Now;
                    opcdet.UActlzcn = HttpContext.User.Identity.Name;
                    OpcionDetalleServ.UpdateOpcionDetalle(opcdet);
                }
                vmListadoOpcionesDetalle lis = BuscarOpcionXModuloXSistema(new vmBuscarOpcionesDetalle { idsistema = Convert.ToInt32(vmOpcDet.ISstma), idModulo = Convert.ToInt32(vmOpcDet.IMdlo), idOpcion = Convert.ToInt32(vmOpcDet.IOpcn) });
                return PartialView("_ListadoOpcionesDetalles", lis);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult EliminarOpcionDetalle(int id)
        {
            OpcionDetalle mod;
            try
            {
                mod = OpcionDetalleServ.GetOpcionDetalle(id);
                mod.CEstdo = "I";
                OpcionDetalleServ.UpdateOpcionDetalle(mod);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { Mensaje = ex.Message });
            }

            vmListadoOpcionesDetalle lis = BuscarOpcionXModuloXSistema(new vmBuscarOpcionesDetalle { idsistema = Convert.ToInt32(mod.ISstma), idModulo = Convert.ToInt32(mod.IMdlo), idOpcion = Convert.ToInt32(mod.IOpcn) });
            return PartialView("_ListadoOpcionesDetalles", lis);

        }
    }
}