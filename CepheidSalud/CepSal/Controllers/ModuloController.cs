﻿using CepSal.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;

namespace CepSal.Controllers
{
    [Authorize(Policy = "TienePermisosModulo")]
    public class ModuloController : Controller
    {
        private readonly IServ_Modulo ModuloServ;
        private readonly IServ_Sistema SistemaServ;

        public ModuloController(IServ_Modulo ModuloServ, IServ_Sistema SistemaServ)
        {
            this.ModuloServ = ModuloServ;
            this.SistemaServ = SistemaServ;
        }
        public List<vmSistema> Buscarsistema()
        {
            var sis = new List<vmSistema>();
            SistemaServ.GetSistemas().ForEach(u =>
            {
                sis.Add(u);
            });
            return sis;
        }
        public vmListadoModulos BuscarModuloXSistema(vmBuscarModulos vm)
        {
            vmListadoModulos lis = new vmListadoModulos();
            lis.Modulos = new List<vmModulo>();
            lis.idsistema = vm.idsistema;
            ModuloServ.ObtenerModuloXSistema(vm.idsistema).ForEach(u =>
            {
                lis.Modulos.Add(u);
            });
            return lis;
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.Sistemas = Buscarsistema();
            vmBuscarModulos vm = new vmBuscarModulos { idsistema = 0 };
            return View(vm);
        }
        [HttpPost]
        public IActionResult BuscarModulosSistema(vmBuscarModulos vm)
        {
            vmListadoModulos lis = BuscarModuloXSistema(vm);

            return PartialView("_ListadoModulos", lis);
        }
        [HttpGet]
        public IActionResult AgregarModulo(int id)
        {
            vmModulo model = new vmModulo(id);
            model.UCrcn = HttpContext.User.Identity.Name;
            model.UActlzcn = HttpContext.User.Identity.Name;
            return PartialView("_AgregarEditarModulo", model);
        }
        [HttpGet]
        public IActionResult EditarModulo(int idMod)
        {
            vmModulo model = ModuloServ.GetModulo(idMod);
            return PartialView("_AgregarEditarModulo", model);
        }
        [HttpGet]
        public IActionResult Modulo(vmModulo item)
        {
            return PartialView("_AgregarEditarModulo", null);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AgregarEditarModulo(vmModulo vmMod)
        {

            try
            {
                Modulo mod = null;
                
                if (vmMod.nId == 0)
                {
                    mod = vmMod;
                    mod.UActlzcn = HttpContext.User.Identity.Name;
                    mod.UCrcn = HttpContext.User.Identity.Name;
                    ModuloServ.InsertModulo(mod);
                }
                else
                {
                    mod = ModuloServ.GetModulo(vmMod.nId);
                    mod.nId = vmMod.nId;
                    mod.ISstma = vmMod.ISstma;
                    mod.IMdlo = vmMod.IMdlo;
                    mod.Nmbre = vmMod.Nmbre;
                    mod.Dscrpcn = vmMod.Dscrpcn;
                    mod.CEstdo = vmMod.CEstdo;
                    mod.FEstdo = vmMod.FEstdo;
                    mod.FActlzcn = DateTime.Now;
                    mod.UActlzcn = HttpContext.User.Identity.Name;

                    ModuloServ.UpdateModulo(mod);
                }
                vmListadoModulos lis = BuscarModuloXSistema(new vmBuscarModulos { idsistema = Convert.ToInt32(vmMod.ISstma) });
                return PartialView("_ListadoModulos", lis);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { Mensaje = ex.Message });
            }
        }
        [HttpGet]
        public IActionResult EliminarModulo(int idMod)
        {
            Modulo mod;
            try
            {
                mod = ModuloServ.GetModulo(idMod);
                mod.CEstdo = "I";
                ModuloServ.UpdateModulo(mod);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { Mensaje = ex.Message });
            }

            vmListadoModulos lis = BuscarModuloXSistema(new vmBuscarModulos { idsistema = Convert.ToInt32(mod.ISstma) });
            return PartialView("_ListadoModulos", lis);
        }
    }
}