﻿using CepSal.Components;
using CepSal.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CepSal.Controllers
{

    [Authorize(Policy = "TienePermisosPerfil")]
    public class PerfilController : Controller
    {
        private readonly IServ_Perfil PerfilServ;
        private readonly IServ_Modulo ModuloServ;
        private readonly IServ_Sistema SistemaServ;
        private readonly IServ_Opcion OpcionServ;
        private readonly IServ_OpcionPerfil OpcionPerServ;
        private readonly IServ_OpcionPerfilDetalle OpcionPerDetServ;
        private readonly IViewRenderService _viewRenderService;

        public PerfilController(IServ_Modulo ModuloServ, IServ_Sistema SistemaServ, IServ_Perfil PerfilServ, IServ_Opcion OpcionServ, IServ_OpcionPerfil OpcionPerServ, IServ_OpcionPerfilDetalle OpcionPerDetServ, IViewRenderService viewRenderService)
        {
            this.ModuloServ = ModuloServ;
            this.SistemaServ = SistemaServ;
            this.PerfilServ = PerfilServ;
            this.OpcionServ = OpcionServ;
            this.OpcionPerServ = OpcionPerServ;
            this.OpcionPerDetServ = OpcionPerDetServ;
            _viewRenderService = viewRenderService;
        }

        public List<vmSistema> Buscarsistema()
        {
            var sis = new List<vmSistema>();
            SistemaServ.GetSistemas().ForEach(u =>
            {
                sis.Add(u);
            });
            return sis;
        }
        public List<vmModulo> Buscarmodulo()
        {
            var mod = new List<vmModulo>();
            ModuloServ.GetModulos().ForEach(u =>
            {
                mod.Add(u);
            });
            return mod;
        }
        public vmListadoPerfiles BuscarPerfilXModuloXSistema(vmBuscarPerfiles vm)
        {
            vmListadoPerfiles lis = new vmListadoPerfiles();
            lis.Perfiles = new List<vmPerfil>();
            lis.idsistema = vm.idsistema;
            lis.idmodulo = vm.idmodulo;
            PerfilServ.ObtenerPerfilXModuloXSistema(vm.idsistema, vm.idmodulo).ForEach(u =>
             {
                 lis.Perfiles.Add(u);
             });
            return lis;
        }
        public vmListadoModulos BuscarModuloXSistemas(int id)
        {
            vmListadoModulos lis = new vmListadoModulos();
            lis.Modulos = new List<vmModulo>();
            lis.idsistema = id;
            ModuloServ.ObtenerModuloXSistema(id).ForEach(u =>
            {
                lis.Modulos.Add(u);
            });
            return lis;
        }
        public bool TienePermisos(int idsistema, int idModulo, int idPer, decimal idOpc)
        {
            var opc = OpcionPerServ.GetOpcionPerfils()
                       .FirstOrDefault(x => x.ISstma == idsistema
                       && x.IMdlo == idModulo
                       && x.IPrfl == idPer
                       && x.IOpcn == idOpc);
            return (opc == null) ? false : (opc.CEstdo == "A") ? true : false;
        }
        public bool DetalleTienePermisos(int idsistema, int idModulo, int idPer, decimal idOpc, decimal idDetOpc)
        {
            var det = OpcionPerDetServ.GetOpcionPerfilDetalles()
                 .FirstOrDefault(x => x.ISstma == idsistema
                       && x.IMdlo == idModulo
                       && x.IPrfl == idPer
                       && x.IOpcn == idOpc
                       && x.IODtlle == idDetOpc);
            return (det == null) ? false : (det.CEstdo == "A") ? true : false;
        }
        public vmArbolOpciones BuscarSistemaXModuloXOpcionesXPerfil(int idsistema, int idModulo, int idPer)
        {
            vmArbolOpciones raiz = new vmArbolOpciones();
            raiz.nodos = new List<Nodos>();
            raiz.idSistema = idsistema;
            raiz.idModulo = idModulo;
            raiz.idPerfil = idPer;

            var opciones = OpcionServ.ObtenerOpcionXModuloXSistema(idsistema, idModulo).ToList();

            for (int i = 0; i < opciones.Count; i++)
            {
                //si es padre agrego como primer nodo
                if (opciones[i].IOPdre == null)
                {
                    raiz.nodos.Add(new Nodos()
                    {
                        id = "opcp-" + opciones[i].IOpcn.ToString(),
                        text = opciones[i].Nmbre,
                        state = new state()
                        {
                            ischecked = TienePermisos(idsistema, idModulo, idPer, opciones[i].IOpcn),
                        },
                        nodes = BuscarHijos(idsistema, idModulo, idPer, opciones[i].IOpcn)
                    });
                }
            }
            return raiz;
        }
        public List<Nodos> BuscarHijos(int idsistema, int idModulo, int idPer, decimal idPadre)
        {
            var nodos = new List<Nodos>();
            var opciones = OpcionServ.ObtenerOpcionXModuloXSistema(idsistema, idModulo).Where(x => x.IOPdre == idPadre).ToList();
            for (int i = 0; i < opciones.Count; i++)
            {
                nodos.Add(new Nodos()
                {
                    id = "opch-" + opciones[i].IOpcn.ToString(),
                    text = opciones[i].Nmbre,
                    state = new state()
                    {
                        ischecked = TienePermisos(idsistema, idModulo, idPer, opciones[i].IOpcn),
                    },
                    nodes = BuscarDetalles(idsistema, idModulo, idPer, opciones[i])
                });
            }
            return (nodos.Count == 0) ? null : nodos;
        }
        public List<Nodos> BuscarDetalles(int idsistema, int idModulo, int idPer, Opcion opc)
        {
            List<Nodos> deta = new List<Nodos>();
            var detalles = opc.OpcionDetalle.ToList();
            for (int j = 0; j < detalles.Count; j++)
            {
                deta.Add(new Nodos
                {
                    id = "opcdet-" + detalles[j].IODtlle.ToString(),
                    text = detalles[j].Nmbre.ToString(),
                    state = new state()
                    {
                        ischecked = DetalleTienePermisos(idsistema, idModulo, idPer, opc.IOpcn, detalles[j].IODtlle),
                    },
                });
            }
            return (deta.Count == 0) ? null : deta;
        }
        public int ConvertToId(string cad)
        {
            var ab = cad.Split("-");
            return Convert.ToInt32(ab[1]);
        }
        public string ConvertToEstado(bool bol)
        {
            if (bol)
                return "A";
            else
                return "I";
        }
        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.Sistemas = Buscarsistema();
            vmBuscarPerfiles vm = new vmBuscarPerfiles { idsistema = 0, idmodulo = 0 };
            return View(vm);
        }

        [HttpGet]
        public JsonResult BuscarModuloXSistema(int id)
        {
            vmListadoModulos lis = BuscarModuloXSistemas(id);
            //ViewBag.Modulos = BuscarModuloXSistemas(id);
            return Json(new SelectList(lis.Modulos, "IMdlo", "Nmbre"));
        }

        [HttpPost]
        public IActionResult BuscarPerfilesXModuloXSistema(vmBuscarPerfiles vm)
        {
            vmListadoPerfiles lis = BuscarPerfilXModuloXSistema(vm);

            return PartialView("_ListadoPerfiles", lis);
        }

        [HttpGet]
        public IActionResult AgregarPerfil(int id, int idM)
        {
            vmPerfil model = new vmPerfil(id, idM);
            model.UCrcn = HttpContext.User.Identity.Name;
            model.UActlzcn = HttpContext.User.Identity.Name;
            return PartialView("_AgregarEditarPerfil", model);
        }

        [HttpGet]
        public IActionResult EditarPerfil(int idPer)
        {
            vmPerfil model = PerfilServ.GetPerfil(idPer);
            return PartialView("_AgregarEditarPerfil", model);
        }

        [HttpGet]
        public IActionResult Perfil(vmPerfil item)
        {
            return PartialView("_AgregarEditarPerfil", null);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AgregarEditarPerfil(vmPerfil vmPer)
        {
            vmListadoPerfiles lis = new vmListadoPerfiles();
            try
            {
                Perfil per = null;
                
                if (vmPer.nId == 0)
                {
                    
                    per = vmPer;
                    per.UCrcn = HttpContext.User.Identity.Name;
                    per.UActlzcn = HttpContext.User.Identity.Name;
                    PerfilServ.InsertPerfil(per);
                }
                else
                {
                    //throw new Exception();
                    per = PerfilServ.GetPerfil(vmPer.nId);
                    per.nId = vmPer.nId;
                    per.ISstma = vmPer.ISstma;
                    per.IMdlo = vmPer.IMdlo;
                    per.Nmbre = vmPer.Nmbre;
                    per.Dscrpcn = vmPer.Dscrpcn;
                    per.CEstdo = vmPer.CEstdo;
                    per.FEstdo = vmPer.FEstdo;
                    per.FActlzcn = DateTime.Now;
                    per.UActlzcn = HttpContext.User.Identity.Name;

                    PerfilServ.UpdatePerfil(per);
                }
                lis = BuscarPerfilXModuloXSistema(new vmBuscarPerfiles { idsistema = Convert.ToInt32(vmPer.ISstma), idmodulo = Convert.ToInt32(vmPer.IMdlo) });
                return PartialView("_ListadoPerfiles", lis);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { Mensaje = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult EliminarPerfil(int id)
        {
            Perfil per;
            try
            {
                per = PerfilServ.GetPerfil(id);
                per.CEstdo = "I";
                PerfilServ.UpdatePerfil(per);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { Mensaje = ex.Message });
            }

            vmListadoPerfiles lis = BuscarPerfilXModuloXSistema(new vmBuscarPerfiles { idsistema = Convert.ToInt32(per.ISstma), idmodulo = Convert.ToInt32(per.IMdlo) });
            return PartialView("_ListadoPerfiles", lis);
        }

        [HttpGet]
        public IActionResult BuscarOpcionesXPerfil(int idsis, int idmod, int idPer)
        {
            vmArbolOpciones raiz = BuscarSistemaXModuloXOpcionesXPerfil(idsis, idmod, idPer);
            return PartialView("_Opciones", raiz);
        }

        [HttpPost]
        public IActionResult GuardarPermisos(vmArbolOpciones arbol)
        {
            List<OpcionPerfil> opciones = new List<OpcionPerfil>();
            //OpcionPerfil opcion = new OpcionPerfil();
            OpcionPerfil opcionPad = new OpcionPerfil();
            OpcionPerfil opcionHij = new OpcionPerfil();

            List<OpcionPerfilDetalle> detalles = new List<OpcionPerfilDetalle>();
            OpcionPerfilDetalle detalle = new OpcionPerfilDetalle();
            foreach (var nodpa in arbol.nodos)
            {//verificar que no exista esta opcion perfil
                //para el padre insertar en opcion perfil
                opcionPad = new OpcionPerfil
                {
                    ISstma = arbol.idSistema,
                    IMdlo = arbol.idModulo,
                    IPrfl = arbol.idPerfil,
                    IOpcn = ConvertToId(nodpa.id),
                    CHbltdo = "S",
                    CEscrtra = "S",
                    CMdfccn="S",
                    UCrcn = HttpContext.User.Identity.Name,
                    UActlzcn = HttpContext.User.Identity.Name,
                    FCrcn = DateTime.Now,
                    FActlzcn = DateTime.Now,
                    FEstdo = DateTime.Now,
                    CEstdo = ConvertToEstado(nodpa.state.ischecked)
                };

                if (nodpa.nodes != null)
                    foreach (var nodhi in nodpa.nodes)
                    {//para los hijos insertar en opcion perfil
                        opcionHij = new OpcionPerfil
                        {
                            ISstma = arbol.idSistema,
                            IMdlo = arbol.idModulo,
                            IPrfl = arbol.idPerfil,
                            IOpcn = ConvertToId(nodhi.id),
                            CHbltdo = "S",
                            CEscrtra = "S",
                            CMdfccn = "S",
                            UCrcn = HttpContext.User.Identity.Name,
                            UActlzcn = HttpContext.User.Identity.Name,
                            FCrcn = DateTime.Now,
                            FActlzcn = DateTime.Now,
                            FEstdo = DateTime.Now,
                            CEstdo = ConvertToEstado(nodhi.state.ischecked)
                        };
                        if (nodhi.nodes != null)
                            foreach (var deta in nodhi.nodes)
                            {//para los detalles insertar en opcion perfil detalle
                                detalle = new OpcionPerfilDetalle
                                {
                                    ISstma = arbol.idSistema,
                                    IMdlo = arbol.idModulo,
                                    IPrfl = arbol.idPerfil,
                                    IOpcn = ConvertToId(nodhi.id),
                                    IODtlle = ConvertToId(deta.id),
                                    CHbltdo = "S",
                                    CEscrtra = "S",
                                    CMdfccn = "S",
                                    UCrcn = HttpContext.User.Identity.Name,
                                    UActlzcn = HttpContext.User.Identity.Name,
                                    FCrcn = DateTime.Now,
                                    FActlzcn = DateTime.Now,
                                    FEstdo = DateTime.Now,
                                    CEstdo = ConvertToEstado(deta.state.ischecked)
                                };
                                detalles.Add(detalle);
                            }
                        ///opcionHij.OpcionPerfilDetalle = detalles;
                        opciones.Add(opcionHij);
                    }
                //opciones.Add(opcionHij);
                opciones.Add(opcionPad);

            }
            OpcionPerServ.InsertOpcionPerfil(opciones, detalles);

            vmArbolOpciones raiz = BuscarSistemaXModuloXOpcionesXPerfil(arbol.idSistema, arbol.idModulo, arbol.idPerfil);
            return PartialView("_Opciones", raiz);
        }
    }
}