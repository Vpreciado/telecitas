﻿using System;

namespace Models
{
    public class OptionItems
    {
        public string value { get; set; }
        public string name { get; set; }
    }
}
