﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Map
{
    public class Map_OpcionPerfil
    {
        
        public Map_OpcionPerfil(EntityTypeBuilder<OpcionPerfil> entity)
        {
            entity.HasKey(e => new { e.ISstma, e.IMdlo, e.IPrfl, e.IOpcn });

            entity.ToTable("SEGSIS05", "SEG");

            entity.HasIndex(e => e.nId)
                .HasName("UC_SIS05_NID")
                .IsUnique();

            entity.Property(e => e.ISstma).HasColumnName("ISstma");

            entity.Property(e => e.IMdlo)
                .HasColumnName("IMdlo")
                .HasColumnType("numeric(10, 0)");

            entity.Property(e => e.IPrfl)
                .HasColumnName("IPrfl")
                .HasColumnType("numeric(2, 0)");

            entity.Property(e => e.IOpcn)
                .HasColumnName("IOpcn")
                .HasColumnType("numeric(4, 0)");

            entity.Property(e => e.CEscrtra)
                .IsRequired()
                .HasColumnName("CEscrtra")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('S')");

            entity.Property(e => e.CEstdo)
                .IsRequired()
                .HasColumnName("CEstdo")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('A')");

            entity.Property(e => e.CHbltdo)
                .IsRequired()
                .HasColumnName("CHbltdo")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('S')");

            entity.Property(e => e.CMdfccn)
                .IsRequired()
                .HasColumnName("CMdfccn")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('S')");

            entity.Property(e => e.FActlzcn)
                .HasColumnName("FActlzcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FCrcn)
                .HasColumnName("FCrcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FEstdo)
                .HasColumnName("FEstdo")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.nId)
                .HasColumnName("NID")
                .ValueGeneratedOnAdd();

            entity.Property(e => e.UActlzcn)
                .IsRequired()
                .HasColumnName("UActlzcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.UCrcn)
                .IsRequired()
                .HasColumnName("UCrcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.HasOne(d => d.Opcion)
                .WithMany(p => p.OpcionPerfil)
                .HasForeignKey(d => new { d.ISstma, d.IMdlo, d.IOpcn })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SEGSIS0500");

            entity.HasOne(d => d.Perfil)
                .WithMany(p => p.OpcionPerfil)
                .HasForeignKey(d => new { d.ISstma, d.IMdlo, d.IPrfl })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SEGSIS0501");
        }
    }
}
