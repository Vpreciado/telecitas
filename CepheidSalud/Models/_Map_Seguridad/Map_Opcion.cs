﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Map
{
    public class Map_Opcion
    {
        
        public Map_Opcion(EntityTypeBuilder<Opcion> entity)
        {
            entity.HasKey(e => new { e.ISstma, e.IMdlo, e.IOpcn });

            entity.ToTable("SEGSIS03", "SEG");

            entity.HasIndex(e => e.nId)
                .HasName("UC_SIS03_NID")
                .IsUnique();

            entity.HasIndex(e => e.URL)
                .HasName("UC_SIS03_URL")
                .IsUnique();

            entity.Property(e => e.ISstma)
                .HasColumnName("ISstma");

            entity.Property(e => e.IMdlo)
                .HasColumnName("IMdlo")
                .HasColumnType("numeric(10, 0)");

            entity.Property(e => e.IOpcn)
                .HasColumnName("IOpcn")
                .HasColumnType("numeric(4, 0)");

            entity.Property(e => e.CEstdo)
                .IsRequired()
                .HasColumnName("CEstdo")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('A')");

            entity.Property(e => e.Dscrpcn)
                .IsRequired()
                .HasMaxLength(250)
                .IsUnicode(false);

            entity.Property(e => e.FActlzcn)
                .HasColumnName("FActlzcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FCrcn)
                .HasColumnName("FCrcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FEstdo)
                .HasColumnName("FEstdo")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.IOPdre)
                .HasColumnName("IOPdre")
                .HasColumnType("numeric(4, 0)");

            entity.Property(e => e.nId)
                .HasColumnName("NID")
                .ValueGeneratedOnAdd();

            entity.Property(e => e.Nmbre)
                .IsRequired()
                .HasMaxLength(150)
                .IsUnicode(false);

            entity.Property(e => e.UActlzcn)
                .IsRequired()
                .HasColumnName("UActlzcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.UCrcn)
                .IsRequired()
                .HasColumnName("UCrcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.URL)
                .HasColumnName("URL")
                .HasMaxLength(250)
                .IsUnicode(false);

            entity.HasOne(d => d.Modulo)
                .WithMany(p => p.Opcion)
                .HasForeignKey(d => new { d.ISstma, d.IMdlo })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SEGSIS03");

            entity.HasOne(d => d.OpcionPadre)
                .WithMany(p => p.OpcionHijo)
                .HasForeignKey(d => new { d.ISstma, d.IMdlo,d.IOPdre })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SEGSIS0303");
        }
    }
}
