﻿namespace Models
{
    public static class IconoBoton
    {
        public const string Editar = "glyphicon glyphicon-pencil";
        public const string Agregar = "glyphicon glyphicon-plus";
        public const string Eliminar = "glyphicon glyphicon-trash";
        public const string Ver = "glyphicon glyphicon-eye-open";
        public const string Cerrar = "glyphicon glyphicon-plus";
    }
    public static class ColorBoton
    {
        public const string Default = "btn btn-default btn-xs";
        public const string Principal = "btn btn-primary btn-xs";
        public const string Info = "btn btn-info btn-xs";
        public const string Exito = "btn btn-success btn-xs";
        public const string Advertencia = "btn btn-warning btn-xs";
        public const string Error = "btn btn-danger btn-xs";
    }
    public static class Tabla
    {
        public const string EstiloTabla = "table table-bordered table-striped table-condensed";
    }
    public static class Codigos
    {
        public const int CEPHEIDSALUD = 1;

        public enum AjaxStatus
        {
            Error=900,
            Exito = 1,
        }
        public enum ComponenteLayout
        {
            PanelUsuario,
            MenuUsuario,
            SideBarMenu
        }
        public enum TamanoModal
        {
            Pequena,
            Grande,
            Mediana
        }
        public enum ColorModal
        {
            Neutro,
            Exito,
            Error,
            Alerta
        }
        public static class Estado
        {
            public const string Activo = "A";
            public const string Inactivo = "I";
        }
        public static class EstadoCivil
        {
            public const string Casado = "C";
            public const string Soltero = "S";
            public const string Divorciado = "D";
            public const string Viudo = "V";
            public const string Conviviente = "J";
        }
        public static class Sexo
        {
            public const string Masculino = "M";
            public const string Femenino = "F";
        }
        public static class TipoPersona
        {
            public const string Natural = "N";
            public const string Juridica = "J";
        }
        public static class TipoDocumento
        {
            public const int _1 = 1;
            public const int _2 = 2;
            public const int _3 = 3;
            public const int _4 = 4;
            public const int _5 = 5;
            public const int _6 = 6;
            public const int _7 = 7;
        }
        public static class Indicador
        {
            public const string Si = "S";
            public const string No = "N";
        }
        public static class TipoDiagnostico
        {
            public const string H = "H";
            public const string P = "P";
        }
        public static class CDASps
        {
            public const string D = "D";
            public const string N = "N";
        }
    }
}
