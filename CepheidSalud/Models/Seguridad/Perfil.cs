﻿using System.Collections.Generic;

namespace Models
{
    public class Perfil:Auditoria
    {
       
        public Perfil()
        {
            this.UsuarioPerfil = new HashSet<UsuarioPerfil>();
            this.OpcionPerfil = new HashSet<OpcionPerfil>();
        }

        public int ISstma { get; set; }
        public decimal IMdlo { get; set; }
        public decimal IPrfl { get; set; }
        public string Nmbre { get; set; }
        public string Dscrpcn { get; set; }

        public virtual ICollection<UsuarioPerfil> UsuarioPerfil { get; set; }
        public virtual Modulo Modulo { get; set; }
        public virtual ICollection<OpcionPerfil> OpcionPerfil { get; set; }
    }
}
