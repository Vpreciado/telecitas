﻿using System.Collections.Generic;

namespace Models
{
    public class Sistema : Auditoria
    {
      
        public Sistema()
        {
            
            this.Modulo = new HashSet<Modulo>();
        }
        /// <summary>
        /// sistema
        /// </summary>
        public int ISstma { get; set; }
        /// <summary>
        /// nombre
        /// </summary>
        public string Nmbre { get; set; }
        /// <summary>
        /// descripcion
        /// </summary>
        public string Dscrpcn { get; set; }

       
        public virtual ICollection<Modulo> Modulo { get; set; }
    }
}
