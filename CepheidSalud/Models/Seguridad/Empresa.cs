﻿using System.Collections.Generic;

namespace Models
{
    public class Empresa : Auditoria
    {
        
        public Empresa()
        {
            this.ErrorSeguridad = new HashSet<ErrorSeguridad>();
            this.Sucursal = new HashSet<Sucursal>();
            this.Usuario = new HashSet<Usuario>();
        }

        /// <summary>
        /// empresa
        /// </summary>
        public int IEmprsa { get; set; }

        /// <summary>
        /// nombre
        /// </summary>
        public string Nmbre { get; set; }
        /// <summary>
        /// descripcion
        /// </summary>
        public string Dscrpcn { get; set; }


        
        public virtual ICollection<ErrorSeguridad> ErrorSeguridad { get; set; }
        public virtual ICollection<Sucursal> Sucursal { get; set; }
        public virtual ICollection<Usuario> Usuario { get; set; }
    }
}
