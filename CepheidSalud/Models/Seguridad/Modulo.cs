﻿using System.Collections.Generic;

namespace Models
{
    public class Modulo : Auditoria
    {
     
        public Modulo()
        {
            
            this.Perfil = new HashSet<Perfil>();
            this.Opcion = new HashSet<Opcion>();
        }
        /// <summary>
        /// sistema
        /// </summary>
        public int ISstma { get; set; }
        /// <summary>
        /// modulo
        /// </summary>
        public decimal IMdlo { get; set; }
        /// <summary>
        /// nombre
        /// </summary>
        public string Nmbre { get; set; }
        /// <summary>
        /// descripcion
        /// </summary>
        public string Dscrpcn { get; set; }


        
        public virtual Sistema Sistema { get; set; }
        public virtual ICollection<Perfil> Perfil { get; set; }
        public virtual ICollection<Opcion> Opcion { get; set; }
    }
}
