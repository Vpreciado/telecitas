﻿using System.Collections.Generic;

namespace Models
{
    public class OpcionDetalle : Auditoria
    {
        
        public OpcionDetalle()
        {
        }
        /// <summary>
        /// sistema
        /// </summary>
        public int ISstma { get; set; }
        /// <summary>
        /// modulo
        /// </summary>
        public decimal IMdlo { get; set; }
        /// <summary>
        /// opcion
        /// </summary>
        public decimal IOpcn { get; set; }
        /// <summary>
        /// opcion detalle
        /// </summary>
        public decimal IODtlle { get; set; }
        /// <summary>
        /// Nombre
        /// </summary>
        public string Nmbre { get; set; }

        public virtual Opcion Opcion { get; set; }

        public ICollection<OpcionPerfilDetalle> OpcionPerfilDetalle { get; set; }
    }
}
