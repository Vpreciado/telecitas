﻿using System.Collections.Generic;

namespace Models
{
    public class Usuario : Auditoria
    {
        
        public Usuario()
        {
            this.EmpresaSucursalUsuario = new HashSet<EmpresaSucursalUsuario>();
            this.UsuarioPerfil = new HashSet<UsuarioPerfil>();
        }
        /// <summary>
        /// empresa
        /// </summary>
        public int IEmprsa { get; set; }
        /// <summary>
        /// persona
        /// </summary>
        public decimal IPrsna { get; set; }
        /// <summary>
        /// tipo usuario
        /// </summary>
        public string TUsro { get; set; }
        /// <summary>
        /// usuario
        /// </summary>
        public string CUsro { get; set; }
        /// <summary>
        /// contraseña
        /// </summary>
        public string Cntrsna { get; set; }
        /// <summary>
        /// creacion contraseña ????? 
        /// </summary>
        public string CCCntrsna { get; set; }
        /// <summary>
        /// fecha creacion contraseña
        /// </summary>
        public System.DateTime FCCntrsna { get; set; }
        /// <summary>
        /// bloqueado
        /// </summary>
        public string CBlqdo { get; set; }
        /// <summary>
        /// fecha bloqueado
        /// </summary>
        public System.DateTime FBlqdo { get; set; }


        public virtual Empresa Empresa { get; set; }
        public virtual ICollection<EmpresaSucursalUsuario> EmpresaSucursalUsuario { get; set; }
        public virtual ICollection<UsuarioPerfil> UsuarioPerfil { get; set; }
    }
}
