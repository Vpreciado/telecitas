﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Map
{
    public class Map_Medico
    {
        public Map_Medico(EntityTypeBuilder<Medico> entity)
        {
            entity.HasKey(e => new { e.nId });

            entity.HasIndex(e => e.IDSYS00)
                  .HasName("UQ_GCSYS02_00")
                  .IsUnique();

            entity.ToTable("GCSYS02", "CIT");

            entity.Property(e => e.nId)
                 .HasColumnName("NID")
                 .ValueGeneratedOnAdd();

            entity.Property(e => e.CMP)
                .HasColumnName("CMP")
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false);

        }
    }
}
