﻿using Microsoft.EntityFrameworkCore;
using Models;
using Repo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repo.Classes
{
    public class RepoErrSeguridad : IRepoErrSeguridad<ErrorSeguridad>
    {
        private readonly AppContext context;
        private DbSet<ErrorSeguridad> entities;

        string errorMessage = string.Empty;

        public RepoErrSeguridad(AppContext context)
        {
            this.context = context;
            entities = context.Set<ErrorSeguridad>();
        }
        public IQueryable<ErrorSeguridad> GetAll()
        {
            return entities;
        }

        public void Insert(ErrorSeguridad entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Add(entity);
            context.SaveChanges();
        }
        public void InsertWithTransaction(ErrorSeguridad entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Commit()
        {
            context.SaveChanges();
        }
    }
}
