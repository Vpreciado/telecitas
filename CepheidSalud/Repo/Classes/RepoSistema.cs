﻿using Microsoft.EntityFrameworkCore;
using Models;
using Repo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repo.Classes
{
   public class RepoSistema : IRepoSistema<Sistema>
    {
        private readonly AppContext context;
        private DbSet<Sistema> entities;

        string errorMessage = string.Empty;

        public RepoSistema(AppContext context)
        {
            this.context = context;
            entities = context.Set<Sistema>();
        }
        public IQueryable<Sistema> GetAll()
        {
            return entities;
        }

        public Sistema Get(int id)
        {
            return entities.SingleOrDefault(s => s.ISstma == id);
        }
        public void Insert(Sistema entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void InsertWithTransaction(Sistema entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(Sistema entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Commit()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
